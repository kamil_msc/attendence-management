import argparse
import csv
import datetime


def get_alpha(s):
    alphanumeric = ""
    for character in s:
        if character.isalnum() or character == ' ' or character == ":" or character == "/":
            alphanumeric += character
    return alphanumeric


def get_master_data(f_name):
    m_data = dict()
    with open(f_name, "rU") as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=",")
        i = 0
        for row in csv_reader:
            if i == 0:
                i = 1
                continue
            row = [entry.decode("utf8") for entry in row]
            s_record = dict()
            s_record['name'] = row[2]
            s_record['roll_no'] = row[1]
            s_record['t_list'] = list()
            s_record['slots'] = list()
            m_data[row[1]] = s_record
    return m_data


def my_function(student):
    return student.get('total_minutes')


def get_data(f_name, master_data, finish_time):
    f = open(f_name)
    lines = list()
    i = 0
    for line in f.readlines():
        if i == 0:
            i = 1
            continue
        i = i + 1
        if i == 10000:
            break
        tokens = line.split(",")
        roll_no = tokens[0]
        if roll_no.find(" ") != -1:
            roll_no = roll_no[0:roll_no.index(" ")]

        l_or_j = tokens[1]

        if master_data.get(roll_no) is None:
            print("Not found ", roll_no)
            continue
        s_record = master_data[roll_no]
        t = tokens[3].replace("\"", "")
        rec = {}
        t_list = s_record['t_list']
        if l_or_j == 'Joined before' or l_or_j == 'Joined':
            rec['action'] = "Joined"
        else:
            rec['action'] = "Left"
        rec['time'] = t
        if len(t_list) == 0:
            t_list.append(rec)
        if len(t_list) > 0:
            prev_rec = t_list[len(t_list)-1]
            if rec['action'] == "Joined" and prev_rec['action'] == "Left":
                t_list.append(rec)
            if rec['action'] == "Left" and prev_rec['action'] == "Joined":
                t_list.append(rec)

    student_list = list()
    for student in master_data:
        s = master_data.get(student)
        calculate(s, finish_time)
        student_list.append(s)

    student_list.sort(key=my_function)
    for stu in student_list:
        print(stu.get('roll_no'), stu.get('total_minutes'), stu.get('t_list'), stu.get('slots'), stu.get('name'))


def get_time_obj(tokens):
    hours = int(tokens[0])
    m = tokens[1]
    s = tokens[2]
    sec = s[0:2]
    pm_am = s[3:]
    if pm_am == "PM" and hours != 12:
        hours = hours + 12
    return datetime.timedelta(hours=int(hours), minutes=int(m), seconds=int(sec), )


def calculate(stu, e_time):
    t_list = stu.get('t_list')
    total_minutes = 0
    length = len(t_list)
    e_t = get_time_obj(e_time.split(":"))
    for i in range(0, length, 2):
        s = t_list[i]['time']
        e = e_time
        if i < length-1:
            e = t_list[i+1]['time']

        s = s[0:-2]
        e = e[0:-2]

        s_tokens = s.split(":")
        e_tokens = e.split(":")
        t_1 = get_time_obj(s_tokens)
        t_2 = get_time_obj(e_tokens)
        if t_1 > e_t:
            continue
        if t_2 > e_t:
            t_2 = e_t
        t_3 = t_2-t_1

        minutes = t_3.seconds/60
        stu['slots'].append(minutes)
        total_minutes = total_minutes + minutes
    stu['total_minutes'] = total_minutes


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', help='Master file contains student name and roll nos')
    parser.add_argument('-d', help='Meeting File')
    parser.add_argument("-e", help='Meeting End Time')
    ns = parser.parse_args()
    master_file = ns.m
    data_file = ns.d
    end_time = ns.e
    if master_file is None or data_file is None:
        parser.error("Please pass arguments properly")
    if end_time is None:
        end_time = "11:00:00 AM"
    master_list = get_master_data(master_file)
    get_data(data_file, master_list, end_time)
