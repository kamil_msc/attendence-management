
def take_no(student):
    return student['no']


def process():
    f = open("SOPBreach/allstudents.csv")
    _d = dict()
    for line in f.readlines():
        s = line.split(",")
        name = s[1].replace("\n", "")
        no = s[0]
        student = {
            "name" : name,
            "no" : s[0],
            "filled": False
        }
        _d[no] = student
    f.close()
    #print(_d)
    f = open("SOPBreach/Filled-Students.csv")
    for line in f.readlines():
        no = line.replace("\n","")
        no = no.replace("\r", "")
        no = no.upper()
        if no in _d:
            student = _d[no]
            student["filled"] = True
    f = open("breached_students.csv", "w")
    unfilled_list = list()
    for no in _d:
        student = _d[no]
        if student['filled'] is False:
            unfilled_list.append(student)
    unfilled_list.sort(key=take_no)
    for student in unfilled_list:
        print(student['name'], student['no'])
        f.write(student['no'] + "    ," + student['name'] + "\n")
    f.close()
    print("Un filled list length ", len(unfilled_list))


process()
