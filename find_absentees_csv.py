import argparse
import csv


def get_alpha(str):
    alphanumeric  = ""
    for character in str:
        if character.isalnum() or character == ' ' or character == ":" or character == "/":
            alphanumeric += character
    return alphanumeric


def get_master_data(f_name, debug=False):
    m_list = list()
    with open(f_name, "rU") as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=",")
        i = 0
        for row in csv_reader:
            i = i + 1
            if i == 1:
                continue
            row = [entry.decode("utf8") for entry in row]
            m_list.append(row[1])
    if debug:
        print("Master List ", m_list)
    return m_list


def get_data(f_name, debug=False):
    d_list = list()
    line = 0
    f = open(f_name)
    for l in f.readlines():
        line = line + 1
        if line == 1 or line == 2:
            if line == 2:
                print(l.replace("\n", ""))
            continue
        l.replace("\0", '')
        t = (l.split(",")[0])
        d_list.append(t)
    if debug:
        print("Data List", d_list)
    return d_list


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-m')
    parser.add_argument('-d')
    parser.add_argument('--debug', help="debug", default=False)
    ns = parser.parse_args()
    master_file = ns.m
    data_file = ns.d
    debug = False
    if ns.debug is not None:
        if ns.debug == "True":
            debug = True
        if ns.debug == "False":
            debug = False
    if master_file is None or data_file is None:
        parser.error("Please pass arguments properly")

    master_list = get_master_data(master_file, debug)
    day_list = get_data(data_file, debug)

    for e in day_list:
        try:
            master_list.remove(e)
        except ValueError:
            pass
    print("Absentees List: No of Absentees ", len(master_list))
    for e in master_list:
        print(e)
