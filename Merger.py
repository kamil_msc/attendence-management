import argparse
import pandas as pd


def get_data(f_name, f):
    xl_file = pd.ExcelFile(f_name)
    s_name = xl_file.sheet_names[0]
    dfs = {
        sheet_name: xl_file.parse(sheet_name)
        for sheet_name in xl_file.sheet_names
    }
    df = dfs.get(s_name)
    _list = list()
    it = df.iterrows()
    next(it)
    next(it)
    next(it)
    next(it)
    for i in range(0, len(df)-4):
        row = next(it)
        txt = str(row)
        #print(txt)
        no = txt[txt.index("Unnamed: 1")+10:txt.index("Unnamed: 2")].replace(" ", "").replace("\n", "")
        name = txt[txt.index("Unnamed: 2")+10:txt.index("Unnamed: 3")].replace("\n", "")
        print(no, name)
        f.write(no + "," + name + "\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    from os import listdir
    from os.path import isfile, join
    folder = "FS"
    files = [f for f in listdir(folder) if isfile(join(folder, f))]
    f = open("allstudents.csv", "w")
    for file in files:
        print(file)
        get_data(folder + "/" + file, f)
    f.close()
