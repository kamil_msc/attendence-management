import argparse
import pandas as pd


def get_data(f_name, print_time=False, col_pos=0):
    xl_file = pd.ExcelFile(f_name)
    s_name = xl_file.sheet_names[0]
    dfs = {
        sheet_name: xl_file.parse(sheet_name)
        for sheet_name in xl_file.sheet_names
    }
    df = dfs.get(s_name)
    _list = list()
    it = df.iterrows()
    for i in range(1, len(df)+1):
        row = next(it)[1]
        if i == 1 and print_time:
            print(row)
        _list.append(row[col_pos])
    return _list


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-m')
    parser.add_argument('-i')
    ns = parser.parse_args()
    master_file = ns.m
    data_file = ns.i
    master_list = get_data(master_file, col_pos=1)
    day_list = get_data(data_file, True, col_pos=0)
    #print("Master List", master_list)
    #print("Day List ", day_list)
    for e in day_list:
        try:
            master_list.remove(e)
        except ValueError:
            pass
    print("Absentees List ", len(master_list))
    for e in master_list:
        print(e)